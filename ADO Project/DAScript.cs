﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoLScriptSeries.EntitityClasses;

namespace LoLScriptSeries.DataAccessClasses
{
    class DAScript : DABaseClass
    {
        public DAScript()
            : base()
        {

        }

        public List<Script> GetAllScripts()
        {
            List<Script> scripts = new List<Script>();

            const string query = "Select * from Script order by naam";
            using (connection)
            {
                OleDbCommand command = new OleDbCommand(query, connection);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Script script = new Script()
                    {
                        Id = (int)reader[0],
                        Naam = reader[1].ToString(),
                        Versie = (int)reader[2],
                        StatusId = (int)reader[3],
                        Picture = reader[4].ToString()
                    };
                    scripts.Add(script);
                }
                reader.Close();
                connection.Close();
            }

            return scripts;
        }

        public Script GetScriptById(int id)
        {
            Script script = null;
            const string query = "Select * From Script where Id = @Id";
            using (connection)
            {
                OleDbCommand command = new OleDbCommand(query, connection);
                command.Parameters.AddWithValue("@Id", id);

                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    script = new Script
                    {
                        Id = (int)reader[0],
                        Naam = reader[1].ToString(),
                        Versie = (int)reader[2],
                        StatusId = (int)reader[3],
                        Picture = reader[4].ToString()
                    };
                }
                reader.Close();
                connection.Close();
            }
            return script;
        }

    }
}
