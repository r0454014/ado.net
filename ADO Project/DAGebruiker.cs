﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoLScriptSeries.EntitityClasses;

namespace LoLScriptSeries.DataAccessClasses
{
    class DAGebruiker : DABaseClass
    {
        public DAGebruiker()
            :base()
        {
        }

        public Boolean CheckGebruiker()
        {
            Boolean success = false;

            Gebruiker gebruiker = new Gebruiker();

            if (GetGebruiker(gebruiker.Hwid))
            {
                const string query = "Update Gebruiker set count = @count where hwid = @hwid";
                using (connection)
                {

                    int count = GetCount(gebruiker.Hwid) + 1;

                    OleDbCommand command = new OleDbCommand(query, connection);
                    command.Parameters.AddWithValue("@hwid", gebruiker.Hwid);
                    command.Parameters.AddWithValue("@count", count);
                    connection.Open();
                    success = command.ExecuteNonQuery() > 0;
                    connection.Close();
                } 
            }
            else
            {
                const string query = "Insert into Gebruiker (hwid, count) values (@hwid, @count)";
                using (connection)
                {
                    OleDbCommand command = new OleDbCommand(query, connection);
                    command.Parameters.AddWithValue("@hwid", gebruiker.Hwid);
                    command.Parameters.AddWithValue("@count", 1);
                    connection.Open();
                    success = command.ExecuteNonQuery() > 0;
                    connection.Close();
                } 
            }
            
            return success;
        }

        public int GetCount(string hwid)
        {
            Gebruiker gebruiker = null;
            const string query = "Select * from Gebruiker where hwid = @hwid";
            using (connection)
            {
                OleDbCommand command = new OleDbCommand(query, connection);
                command.Parameters.AddWithValue("@hwid", hwid);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    gebruiker = new Gebruiker()
                    {
                        Count = (int)reader[2]
                    };
                }
                reader.Close();
                connection.Close();
            }

            return gebruiker.Count;
        }

        public Boolean GetGebruiker(string hwid)
        {
            Boolean isSuccess = false;
            const string query = "Select * from Gebruiker where hwid = @hwid";
            using (connection)
            {
                OleDbCommand command = new OleDbCommand(query, connection);
                command.Parameters.AddWithValue("@hwid", hwid);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    isSuccess = true;
                }
                reader.Close();
                connection.Close();
            }

            return isSuccess;
        }
    }
}
