﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Connectionstring uit App.config
using System.Configuration;
// SQL Server Data Provider gebruiken
using System.Data.SqlClient;
using SortOrder = System.Windows.Forms.SortOrder;


namespace Contactpersonen
{
    public partial class FormContactpersonen : Form
    {
        // module variabele 
        // connectionString uit App.config
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["local"].ConnectionString;
        private int id;
        private Boolean sort = false;

        public FormContactpersonen()
        {
            InitializeComponent();
            InitializeComponentExtra();
        }

        private void InitializeComponentExtra()
        {
            listViewContactpersonen.BeginUpdate();
            listViewContactpersonen.View = View.Details;
            listViewContactpersonen.FullRowSelect = true;
            listViewContactpersonen.MultiSelect = false;
            listViewContactpersonen.SmallImageList = ImageListIcons;
            // twee kolommen in ListView
            
            listViewContactpersonen.Columns.Add("Naam", "Naam", 110, HorizontalAlignment.Left, 3);
            listViewContactpersonen.Columns.Add("Email", 160, HorizontalAlignment.Left);

            listViewContactpersonen.EndUpdate();

        }


        private void FormContactpersonen_Load(object sender, EventArgs e)
        {
            LeesAantalContactpersonen();
            LeesContactpersonen();
        }

        private void LeesAantalContactpersonen()
        {
            // een connectie-object maken
            SqlConnection connection = new SqlConnection(connectionString);
            // connectie openen
            connection.Open();
            // sql-statement uitvoeren met command-object
            SqlCommand command = new SqlCommand(
                "Select count(*) from Contact", connection);
            // slechts 1 waarde als resultaat, dus ExecuteScalar
            int aantal = (int)(command.ExecuteScalar());
            labelAantal.Text = aantal.ToString() + " records";
            // connectie sluiten
            connection.Close();
        }

        private void LeesContactpersonen()
        {
            listViewContactpersonen.BeginUpdate();
            // connectie-object maken en connectie openen
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            // SQL select-statement uitvoeren
            SqlCommand command = new SqlCommand(
                "Select * from Contact order by Naam", connection);
            // DataReader want Select geeft een resultset: ExecuteReader
            SqlDataReader dataReader = command.ExecuteReader();
            // zolang er contactpersonen zijn
            while (dataReader.Read())
            {
                // nieuwe ListItems toevoegen
                ListViewItem item = new ListViewItem(dataReader["Naam"].ToString(), 1) {Tag = dataReader["ID"].ToString()};
                item.SubItems.Add(dataReader["Email"].ToString());
                listViewContactpersonen.Items.Add(item);
            }
            // datareader en connectie sluiten
            dataReader.Close();
            connection.Close();

            listViewContactpersonen.EndUpdate();
        }

        private void LeesContactpersoon()
        {
            // connectie-object maken en connectie openen
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand(
                "Select * from Contact where id = @id", connection);
            // parameters doorgeven met Add()-methode van Parame-
            // terscollectie
            command.Parameters.Add(new SqlParameter("@id", id));
            // DataReader want Select geeft resultset met 0 of 1 record
            SqlDataReader dataReader = command.ExecuteReader();
            // 1 keer Read() om te positioneren op eerste record 
            // (als die er is)
            if (dataReader.Read())
            {
                textBoxNaam.Text = dataReader["Naam"].ToString();
                textBoxEmail.Text = dataReader["Email"].ToString();
            }
            // datareader en connectie sluiten
            dataReader.Close();
            connection.Close();
        }


        private void listViewContactpersonen_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView view = (ListView)sender;
            view.Focus();
            // werd er een contactpersoon geselecteerd ?
            if (listViewContactpersonen.SelectedItems.Count != 0)
            {
                // id ophalen van geselecteerde contactpersoon
                id = Convert.ToInt32(listViewContactpersonen.SelectedItems[0].Tag);
                LeesContactpersoon();
            }
            else
            {
                textBoxNaam.Clear();
                textBoxEmail.Clear();
            }

        }

        private void buttonWijzigen_Click(object sender, EventArgs e)
        {
            SchrijfContactpersoon();
            // in "echte" applicatie beter alleen de aangepaste 
            // gegevens wijzigen ipv alles terug in te lezen
            listViewContactpersonen.Items.Clear();
            LeesContactpersonen();

        }

        private void SchrijfContactpersoon()
        {
            // connectie-object maken en connectie openen
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            // update-statement met command-object uitvoeren
            SqlCommand command = new SqlCommand("Update Contact set naam = @naam, email = @email where id = @id", connection);
            // Parameters doorgeven met Add()-methode van //Parameters-collectie
            // Let op: dezelfde volgorde gebruiken als voorkomen in // SQL-statement
            command.Parameters.Add(new SqlParameter("@naam", textBoxNaam.Text));
            command.Parameters.Add(new SqlParameter("@email", textBoxEmail.Text));
            command.Parameters.Add(new SqlParameter("@id", id));
            // update-statement uitvoeren
            command.ExecuteNonQuery();
            connection.Close();
        }

        private void SchrijfNieuwContactPersoon()
        {
            // connectie-object maken en connectie openen
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            // update-statement met command-object uitvoeren
            SqlCommand command = new SqlCommand("Insert into Contact (Naam, Email) values (@naam,  @email)", connection);
            // Parameters doorgeven met Add()-methode van //Parameters-collectie
            // Let op: dezelfde volgorde gebruiken als voorkomen in // SQL-statement
            command.Parameters.Add(new SqlParameter("@naam", textBoxNaam.Text));
            command.Parameters.Add(new SqlParameter("@email", textBoxEmail.Text));
            // update-statement uitvoeren
            command.ExecuteNonQuery();
            connection.Close();
        }

        private void ClearTextBoxes()
        {
            textBoxEmail.Clear();
            textBoxNaam.Clear();
        }

        private void buttonToevoegen_Click(object sender, EventArgs e)
        {
            SchrijfNieuwContactPersoon();
            listViewContactpersonen.Items.Clear();
            LeesAantalContactpersonen();
            LeesContactpersonen();
            ClearTextBoxes();
        }

        private void buttonSchrappen_Click(object sender, EventArgs e)
        {
            VerwijderContactpersoon();
            listViewContactpersonen.Items.Clear();
            LeesAantalContactpersonen();
            LeesContactpersonen();
            ClearTextBoxes();

        }

        private void VerwijderContactpersoon()
        {
            // connectie-object maken en connectie openen
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            // update-statement met command-object uitvoeren
            SqlCommand command = new SqlCommand("Delete from Contact where Naam = @naam and Email = @email", connection);
            // Parameters doorgeven met Add()-methode van //Parameters-collectie
            // Let op: dezelfde volgorde gebruiken als voorkomen in // SQL-statement
            command.Parameters.Add(new SqlParameter("@naam", textBoxNaam.Text));
            command.Parameters.Add(new SqlParameter("@email", textBoxEmail.Text));
            // update-statement uitvoeren
            command.ExecuteNonQuery();
            connection.Close();
        }

        private void buttonZoeken_Click(object sender, EventArgs e)
        {
            FormZoek form = new FormZoek();
            form.ShowDialog();

            if (form.DialogResult != DialogResult.OK) return;
            string naam = form.Naam.ToLower();
            textBoxNaam.Text = naam;

            for (var index = 0; index < listViewContactpersonen.Items.Count; index++)
            {
                ListViewItem zoek = listViewContactpersonen.Items[index];
                if (zoek.SubItems[1].Text.ToLower().Contains(naam))
                {
                    zoek.Selected = true;
                    break;
                }
            }
        }

        private void listViewContactpersonen_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                if (sort == false)
                {
                    listViewContactpersonen.Sorting = SortOrder.Descending;
                    sort = true;
                    listViewContactpersonen.Columns[0].ImageIndex = 4;
                } else {
                    listViewContactpersonen.Sorting = SortOrder.Ascending;
                    sort = false;
                    listViewContactpersonen.Columns[0].ImageIndex = 3;
                }
            }
        }

    }
}
