﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contactpersonen
{
    public partial class FormZoek : Form
    {
        private string naam;
        public string Naam {
            get { return naam; }
        }

        public FormZoek()
        {
            InitializeComponent();
        }

        private void FormZoek_Load(object sender, EventArgs e)
        {
            Text = "Zoek";
            buttonOK.Text = "OK";
            buttonAnnuleer.Text = "Annuleer";
            buttonAnnuleer.DialogResult = DialogResult.Cancel;
            buttonOK.DialogResult = DialogResult.OK;
            this.CenterToParent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            naam = textBoxZoek.Text;
        }
    }
}
